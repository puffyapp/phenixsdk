# Phenix iOS SDK

The Phenix iOS SDK provides APIs that enable iOS devices to stream from and to Phenix PCast.

The SDK utilizes the iOS SDK and provices high level APIs to publish and view streams.
