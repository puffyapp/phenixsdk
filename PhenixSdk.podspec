#
# Be sure to run `pod lib lint PhenixSdk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name                = "PhenixSdk"
  s.version             = "2018.2.2"
  s.summary             = "Phenix Real-Time Video SDK."
  s.homepage            = "https://phenixrts.com"
  s.license             = { :type => 'MIT', :file => 'LICENSE' }
  s.author              = { "Phenix" => "info@phenixrts.com" }
  s.vendored_frameworks = "lib/Universal/PhenixSdk.framework"
  s.source              = { :git => "https://bitbucket.org/puffyapp/phenixsdk.git" }
  s.source_files        = "lib/Universal/PhenixSdk.framework/Headers/**/*.h"
  s.public_header_files = "lib/Universal/PhenixSdk.framework/Headers/**/*.h"
  s.preserve_paths      = "lib/Universal/PhenixSdk.framework"
  s.platform            = :ios, '9.0'
  s.requires_arc        = true
  s.frameworks          = 'UIKit','Foundation','CoreFoundation'
  s.module_name         = 'PhenixSdk'
end
